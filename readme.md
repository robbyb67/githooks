# Git helper hooks
## prepare-commit-msg

Tries to extract Jira ticket number from current branch name and use that to prefix the commit message.

Supported branch name pattern:

    branchtype/XXX-12345-Descriptive_branch_name

Effect:

    git commit -m "commit message"

will be transformed to "xxx-12345: commit message"

Prerequisites: Bash & awk installed

Install: copy to 

    projectdir/.git/hooks directory